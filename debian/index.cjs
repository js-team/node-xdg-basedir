const { xdgData, xdgConfig, xdgCache, xdgRuntime, xdgState, xdgDataDirectories, xdgConfigDirectories} = require('./dhnodejsBundle.cjs');

const res = {
    // Old API
    data: xdgData,
    config: xdgConfig,
    cache: xdgCache,
    runtime: xdgRuntime,
    dataDirs: xdgDataDirectories,
    configDirs: xdgConfigDirectories,
    // New API
    xdgData: xdgData,
    xdgConfig: xdgConfig,
    xdgCache: xdgCache,
    xdgRuntime: xdgRuntime,
    xdgDataDirectories: xdgDataDirectories,
    xdgState: xdgState,
    xdgConfigDirectories: xdgConfigDirectories,
};

module.exports = res;
